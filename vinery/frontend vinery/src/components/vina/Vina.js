import React from "react";
import { Table, Button, Form, ButtonGroup,Row, Col } from "react-bootstrap";
import { withParams, withNavigation } from "../../routeconf";
import AppAxios from "../../apis/AppAxios";

class Vina extends React.Component {
  constructor(props) {
    super(props);
//ovde definises pocetne parametre za search koji su ti potrebni
   const search = {
      vinarijaId: "",
      nazivVina: "",
      pageNo: 0,
    };

 
//u state pozivas pocetne vrednosti create, search koje si gore definisala i mapu linija i prevoznika koje ces dobiti iz baze
    this.state = {
      vina: [],
      vinarije: [],
      tipovi: [],
      buyGet: 0,
      pageNo: 0,
      totalPages: 2,
      search: search
    };
  }

 
  componentDidMount() {
    this.getData();
  }

  async getData() {
    await this.getVina(0);
    await this.getVinarije();
    await this.getTipVina()
  }

  async getVina(newPageNo) {
      let config = { 
      params: {
        vinarijaId: this.state.search.vinarijaId,
    
        nazivVina: this.state.search.nazivVina,
        pageNo: newPageNo,
    } 
  };

    AppAxios.get('/vina', config)
    .then(res => {
        console.log(res);
        this.setState({
            vina: res.data,
            pageNo: newPageNo,
            totalPages : res.headers['total-pages']
        });
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Error occured please try again!');
    });
}

//ovde dobavljas prevoznike
getVinarije(){
  AppAxios.get("/vinarije")
  .then((response)=>{
      this.setState({vinarije:response.data});
  })
  .catch((err=>{console.log(err)}));
}

getTipVina(){
    AppAxios.get("/tipovi")
    .then((response)=>{
        this.setState({tipovi:response.data});
    })
    .catch((err=>{console.log(err)}));
  }

deleteFromState(vinoId) {
  var vina = this.state.vina;
  vina.forEach((element, index) => {
      if (element.id === vinoId) {
        vina.splice(index, 1);
          this.setState({ vina: vina });
      }
  });
}

delete(vinoId) {
  AppAxios.delete('/vina/' + vinoId)
      .then(res => {
          // handle success
          console.log(res);
          alert('Product was deleted successfully!');
          this.deleteFromState(vinoId); // ili refresh page-a window.location.reload();
      })
      .catch(error => {
          // handle error
          console.log(error);
          alert('Error occured please try again!');
      });
}


onInputChange(event) {
  const name = event.target.name;
  const value = event.target.value

  let search = this.state.search;
  search[name] = value;

  this.setState({ search })
}

goToAdd() {
  this.props.navigate("/vina/add");
}

onNumberChange(event) {
  console.log(event.target.value);

  const { name, value } = event.target;
  console.log(name + ", " + value);

  this.setState((state, props) => ({
      number: value}));
}

buyThis(id) {
var params = {
  'vino': id,
  'kolicina' : this.state.number

}
console.log(params)

    AppAxios.post("/vina", params)
    .then((res)=>{
        this.getVina()
        this.props.navigate("/vina")
        console.log(res)
    })
    .catch((err=>{
        alert("Nedovoljan broj flasa na stanju")
        this.props.navigate("/vina")
        console.log(err)}));

}

renderVina() {
  return this.state.vina.map((vino) => {

      let vinoId = vino.id;

      return (
          <tr key={vinoId}>
              <td>{vino.naziv}</td>
              <td>{vino.vinarijaNaziv}</td>
              <td>{vino.tipVina}</td>
              <td>{vino.opis}</td>
              <td>{vino.godinaProizvodnje}</td>
              <td>{vino.cenaFlase}</td>
              <td>{vino.dostupno}</td>
              {window.localStorage['role'] == 'ROLE_ADMIN' ?
                  [<td><Form.Group>
                      <Form.Control
                          name="number"
                          as="input"
                          type="number"
                          placeholder='kolicina'
                          min={0}
                          onInput={(e) => this.onNumberChange(e)}></Form.Control>
                  </Form.Group></td>,
              <td><Button variant="success" onClick={() => this.addThis(vino)}>Nabavi</Button></td>,
              <td><Button variant="danger" onClick={() => this.delete(vinoId)}>Obrisi</Button></td>
              ]:
              [<td><Form.Group>
                  <Form.Control
                      name="number"
                      as="input"
                      type="number"
                      placeholder='kolicina'
                      min={0}
                      onInput={(e) => this.onNumberChange(e)}></Form.Control>
              </Form.Group></td>,
          <td><Button variant="success" onClick={() => this.buyThis(vinoId)}>Kupi</Button></td>]
              }
          </tr>
      )
  })
}

renderSearchForm() {
  return (
      <>
      <Form style={{ width: "100%" }}>
          <Row><Col>
              <Form.Group>
                  <Form.Label>Naziv vina</Form.Label>
                  <Form.Control
                      name="nazivVina"
                      as="input"
                      type="text"
                      onChange={(e) => this.onInputChange(e)}></Form.Control>
              </Form.Group>
          </Col></Row>

          <Row><Col>
              <Form.Group>
                  <Form.Label>Vinarija</Form.Label>
                  <Form.Control name="vinarijaId" 
                  as="select"
                  type="number"
                  onChange={(e)=>this.onInputChange(e)}>
                      <option value=""></option>
                      {this.state.vinarije.map((vinarije)=>{
                          return(
                              <option value={vinarije.id}>{vinarije.naziv}</option>
                          );
                      })}
                  </Form.Control>
              </Form.Group>
          </Col></Row>
      </Form>
      <Row><Col>
          <Button className="mt-3" onClick={() => this.getVina()}>Pretrazi</Button>
      </Col></Row>
      </>
  );
}
render() {
  return (
      <Col>
          <Row><h1>Vina</h1></Row>

          <Row>
              {this.renderSearchForm()}
          </Row>
          <br/>

          {window.localStorage['role']=='ROLE_ADMIN'?
          <Row><Col>
          <Button onClick={() => this.goToAdd() }>Dodaj</Button>
          </Col></Row>: null}

          <Row><Col>
          <Table style={{ width: "100%" }}>
          <thead>
          <tr>
          <th>Naziv vina</th>
          <th>Vinarija</th>
          <th>Tip</th>
          <th>Opis</th>
          <th>Godina proizvodnje</th>
          <th>Cena po flasi</th>
          <th>Broj preostalih flasa</th>
          <th></th>
          <th></th>
          <th></th>
          </tr>
          </thead>
          <tbody>
              {this.renderVina()}
          </tbody>
          </Table>
          </Col></Row>
          <Row>
              <Col>
              <Button disabled={this.state.pageNo===0} 
                onClick={()=>this.getVina(this.state.pageNo-1)}
                className="mr-3">Prethodna</Button>
              <Button disabled={this.state.pageNo==this.state.totalPages-1} 
              onClick={()=>this.getVina(this.state.pageNo+1)}>Sledeca</Button>
              </Col>
          </Row>
      </Col>
  );
}
}


    export default withNavigation(withParams(Vina));