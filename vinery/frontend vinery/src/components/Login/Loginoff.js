import React from "react";
import { Form, Button, Row, Col} from "react-bootstrap";

import {login} from '../../services/auth';
import AppAxios from "../../apis/AppAxios";

class Login extends React.Component {
  constructor(props) {
    super(props);

    let change = 
    {
      username: "",
      password: ""
    };


    this.state = { change:change };
  }
  
  

  onInputChange(event) {
    let control = event.target;

    let name = control.name;
    let value = control.value;

    let change = this.state.change;
    change[name] = value;
    this.setState(change);
  }

  doLogin(){
      var params = {
        'username': this.state.username,
        'password' : this.state.password
      
      }
      console.log(params)
      
          AppAxios.post("/korisnici/auth", params)
          .then((res)=>{
            this.props.navigate("/proizvodi")
              console.log(res)
          })
          .catch((err=>{
              alert("NIje uspeo login")
              window.location.reload(true);
              
              console.log(err)}));
      
      }
  

  // TODO: Ulepšati: - centrirati, udaljiti od vrha, staviti jumbotron
  // TODO: Završiti implementaciju
  render() {
    return (
      <Row className="justify-content-center">
        <Col md={6}>        
          <Form>
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control type="text" name="username" onChange = {(e) => this.onInputChange(e)}/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" name="password" onChange = {(e) => this.onInputChange(e)}/>
            </Form.Group>
            <Button variant="success" onClick={() => this.doLogin()}>Log in</Button>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default Login;
