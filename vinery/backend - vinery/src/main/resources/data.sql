INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
      
INSERT INTO tip_vina (naziv) VALUES ('Suvo belo vino');
INSERT INTO tip_vina (naziv) VALUES ('Poluslatko belo vino');
INSERT INTO tip_vina (naziv) VALUES ('Suvo crveno vino');
              
INSERT INTO vinarija (naziv, godina_osnivanja) VALUES ('Vinarija Zvonko Bogdan', 2010);
INSERT INTO vinarija (naziv, godina_osnivanja) VALUES ('Vinarija Draskovic', 2008);
INSERT INTO vinarija (naziv, godina_osnivanja) VALUES ('Vinarija Kovacevic', 2009);

INSERT INTO vino (naziv, opis, godina_proizvodnje, cena_flase, dostupno, tip_vina_id, vinarija_id) 
			VALUES ('8 tamburasa', 'Lagano belo vino',2015, 1200, 25, 1, 1);
INSERT INTO vino (naziv, opis, godina_proizvodnje, cena_flase, dostupno, tip_vina_id, vinarija_id) 
			VALUES ('Muskat ottonel', 'Vino sa posebnog teroara',2016, 700, 50, 2, 2);
INSERT INTO vino (naziv, opis, godina_proizvodnje, cena_flase, dostupno, tip_vina_id, vinarija_id) 
			VALUES ('Zivot tece', 'Skladno vino stvoreno od merloa',2010, 900, 100, 3, 1);