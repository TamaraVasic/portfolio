package com.ftninformatika.jwd.modul3.test.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;
import com.ftninformatika.jwd.modul3.test.support.VinarijaDtoToVinarija;
import com.ftninformatika.jwd.modul3.test.support.VinarijaToVinarijaDto;
import com.ftninformatika.jwd.modul3.test.web.dto.TipVinaDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/vinarije",produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class VinarijaController {
	
	@Autowired
	private VinarijaService vinarijaService;
	
	@Autowired
	private VinarijaToVinarijaDto toVinarijaDto;
	
	@Autowired
	private VinarijaDtoToVinarija toVinarija;
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public	ResponseEntity<List<VinarijaDTO>>getAll() {
		List<Vinarija> vinarijaList = vinarijaService.findAll();
		return new ResponseEntity<>(toVinarijaDto.convert(vinarijaList),HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<VinarijaDTO> getOne(@PathVariable Long id){
		Vinarija vinarija = vinarijaService.findOne(id);
			return new ResponseEntity<>(toVinarijaDto.convert(vinarija),HttpStatus.OK);
		}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
	    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	
}
	

   