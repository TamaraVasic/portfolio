package com.ftninformatika.jwd.modul3.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.service.VinoService;
import com.ftninformatika.jwd.modul3.test.support.VinoDtoToVino;
import com.ftninformatika.jwd.modul3.test.support.VinoToVinoDto;
import com.ftninformatika.jwd.modul3.test.web.dto.VinoDTO;

@RestController
@RequestMapping(value="/api/vina")
public class VinoController {
	
	@Autowired
	private VinoService vinoService;
	
	@Autowired
	private VinoToVinoDto toVinoDto;
	
	@Autowired
	private VinoDtoToVino toVino;
	
    @PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping ("/{id}")
	public ResponseEntity<VinoDTO> getOne(@PathVariable Long id) {
		System.out.println(id);
		Vino vino = vinoService.findOnebyId(id);
		if (vino == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		else {
		return new ResponseEntity<>(toVinoDto.convert(vino), HttpStatus.OK);
	}
	}
	 
    @PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<VinoDTO>> getAll(	         
	            @RequestParam(required=false) Long vinarijaId,
	            @RequestParam(required=false) String nazivVina,
	            @RequestParam(required=false,value = "pageNo", defaultValue = "0") int pageNo){
		   
	        Page<Vino> page = vinoService.find(vinarijaId,nazivVina,pageNo);

	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

	        return new ResponseEntity<List<VinoDTO>>(toVinoDto.convert(page.getContent()),headers, HttpStatus.OK);
	    }
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<VinoDTO> create(@Valid @RequestBody VinoDTO vinoDTO) {	
			Vino sacuvanoVino = vinoService.save(toVino.convert(vinoDTO));
			return new ResponseEntity<VinoDTO>(toVinoDto.convert(sacuvanoVino), HttpStatus.CREATED);
		}
	 
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value="/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<VinoDTO> update(@PathVariable Long id, @Valid @RequestBody VinoDTO vinoDTO){
	        if(!id.equals(vinoDTO.getId())) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        Vino vino = toVino.convert(vinoDTO);
	        Vino sacuvanoVino = vinoService.update(vino);
	        
	        return new ResponseEntity<>(toVinoDto.convert(sacuvanoVino),HttpStatus.OK);
	  }
		
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}") 		
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Vino obrisanoVino = vinoService.delete(id);
			if (obrisanoVino!=null) {			
			return new ResponseEntity<> (HttpStatus.NO_CONTENT);			
			} else {						
			return new ResponseEntity<> (HttpStatus.NOT_FOUND);
			}
	  }
}

