package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.web.dto.TipVinaDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;

@Component
public class TipVinaToTipVinaDto implements Converter<TipVina, TipVinaDTO> {

    @Override
    public TipVinaDTO convert(TipVina source) {
    	TipVinaDTO dto = new TipVinaDTO();
    	dto.setId(source.getId());
    	dto.setNaziv(source.getNaziv());

     
        return dto;
    }

    public List<TipVinaDTO> convert(List<TipVina> kategorije){
        List<TipVinaDTO> pDto = new ArrayList<>();

        for(TipVina p : kategorije) {
        	pDto.add(convert(p));
        }

        return pDto;
    }

}