package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.repository.TipVinaRepository;
import com.ftninformatika.jwd.modul3.test.repository.VinarijaRepository;
import com.ftninformatika.jwd.modul3.test.service.TipVinaService;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;


@Service
@Transactional
public class JpaTipVinaService implements TipVinaService {
	
	@Autowired
	private TipVinaRepository tipVinaRepository;


	@Override
	public List<TipVina> findAll() {
		// TODO Auto-generated method stub
		return tipVinaRepository.findAll();
	}

	@Override
	public TipVina findOnebyID(Long id) {
		Optional<TipVina> tipVina = tipVinaRepository.findById(id);

		if (tipVina.isPresent()) {
			return tipVina.get();
		}

		return null;
	}

	}



	