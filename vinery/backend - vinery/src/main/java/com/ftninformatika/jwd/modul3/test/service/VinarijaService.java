package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;



public interface VinarijaService {
	


	List<Vinarija> findAll();

	Vinarija findOne(Long id);



	
	
}