package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;

@Component
public class VinarijaToVinarijaDto implements Converter<Vinarija, VinarijaDTO> {

    @Override
    public VinarijaDTO convert(Vinarija source) {
    	VinarijaDTO dto = new VinarijaDTO();
    	dto.setId(source.getId());
    	dto.setNaziv(source.getNaziv());
    	dto.setGodinaOsnivanja(source.getGodinaOsnivanja());
//    	dto.setProizvodId(source.getProizvod().getId());
     
        return dto;
    }

    public List<VinarijaDTO> convert(List<Vinarija> vinarije){
        List<VinarijaDTO> vDto = new ArrayList<>();

        for(Vinarija v : vinarije) {
        	vDto.add(convert(v));
        }

        return vDto;
    }

}