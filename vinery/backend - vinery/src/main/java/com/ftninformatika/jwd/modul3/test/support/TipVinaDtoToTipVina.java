package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.service.TipVinaService;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;
import com.ftninformatika.jwd.modul3.test.web.dto.TipVinaDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;


@Component
public class TipVinaDtoToTipVina implements Converter<TipVinaDTO, TipVina> {

    @Autowired
    private TipVinaService kategorijaService;

    @Override
    public TipVina convert(TipVinaDTO sourceDto) {
    	
    	TipVina entity = null;
    	
    	if(sourceDto.getId() == null) {
            entity = new TipVina() ;
		}
    	else {
    		entity = kategorijaService.findOnebyID(sourceDto.getId());
		}
    	
    	if ( entity  !=null) {
    		entity.setId(sourceDto.getId());
    		entity.setNaziv(sourceDto.getNaziv());
   
    }
		return entity;
}
}