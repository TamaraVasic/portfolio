package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.service.impl.JpaVinoService;



public interface VinoService {
	
	Vino findOnebyId(Long id);
	
	Vino save(Vino vino);
	
	Vino update(Vino vino);
	
	Vino delete(Long id);
	
Page<Vino> find(Long vinarijaId, String nazivVina, int pageNo);

	
}