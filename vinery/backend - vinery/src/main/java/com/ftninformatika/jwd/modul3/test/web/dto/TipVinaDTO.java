package com.ftninformatika.jwd.modul3.test.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.ftninformatika.jwd.modul3.test.model.Vino;

import java.util.*;

public class TipVinaDTO {

    //@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;

//    @NotBlank(message = "Naziv kategorije nije zadat.")
    private String naziv;
    
    List<VinoDTO> vina;

	public TipVinaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public TipVinaDTO(Long id, String naziv, List<VinoDTO> vina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vina = vina;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<VinoDTO> getVina() {
		return vina;
	}

	public void setVina(List<VinoDTO> vina) {
		this.vina = vina;
	}
}
   
    
    
    