package com.ftninformatika.jwd.modul3.test.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;

public class VinarijaDTO {

    //@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;

//    @NotBlank(message = "Naziv prevoznika nije zadat.")
    private String naziv;
    
    private Integer godinaOsnivanja;
    
   

	List<VinoDTO> vina;

	public VinarijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VinarijaDTO(Long id, String naziv, Integer godinaOsnivanja, List<VinoDTO> vina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.godinaOsnivanja = godinaOsnivanja;
		this.vina = vina;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Integer getGodinaOsnivanja() {
		return godinaOsnivanja;
	}

	public void setGodinaOsnivanja(Integer godinaOsnivanja) {
		this.godinaOsnivanja = godinaOsnivanja;
	}

	public List<VinoDTO> getVina() {
		return vina;
	}

	public void setVina(List<VinoDTO> vina) {
		this.vina = vina;
	}
	
}