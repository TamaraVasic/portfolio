package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.repository.VinoRepository;
import com.ftninformatika.jwd.modul3.test.repository.VinarijaRepository;
import com.ftninformatika.jwd.modul3.test.service.VinoService;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;


@Service
@Transactional
public class JpaVinoService implements VinoService {
	@Autowired
	private VinoRepository	vinoRepository;



	@Override
	public Vino save(Vino vino) {
		// TODO Auto-generated method stub
		return vinoRepository.save(vino);
	}

	@Override
	public Vino findOnebyId(Long id) {
		Optional<Vino> vino = vinoRepository.findById(id);
		if (vino.isPresent()) {
			return vino.get();
		}
		return null;
	}



	@Override
	public Vino delete(Long id) {
	
     Optional<Vino> vino = vinoRepository.findById(id);
		
		if(vino.isPresent()) {
			
			vinoRepository.deleteById(id);
				
		return vino.get();
	}
		return null;
	}


	@Override
	public Page<Vino> find(Long vinarijaId, String nazivVina, int pageNo) {
		if (nazivVina == null) {
			nazivVina = "";
		}
		
		if (vinarijaId == null) {
			return vinoRepository.findByNazivIgnoreCaseContains( nazivVina, PageRequest.of(pageNo, 3));
		}
		
		return vinoRepository.findByVinarijaIdAndNazivIgnoreCaseContains(vinarijaId, nazivVina, PageRequest.of(pageNo, 3));
		
	}


	@Override
	public Vino update(Vino vino) {
		// TODO Auto-generated method stub
		return vinoRepository.save(vino);
	}

}

	