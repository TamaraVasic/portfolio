package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;
import com.ftninformatika.jwd.modul3.test.service.VinoService;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;


@Component
public class VinarijaDtoToVinarija implements Converter<VinarijaDTO, Vinarija> {

    @Autowired
    private VinarijaService vinarijaService;
    
    @Autowired
    private VinoService vinoService;

    @Override
    public Vinarija convert(VinarijaDTO sourceDto) {
    	
    	Vinarija entity = null;
    	
    	if(sourceDto.getId() == null) {
    		entity = new Vinarija() ;
		}
    	else {
    		entity = vinarijaService.findOne(sourceDto.getId());
		}
    	
    	if ( entity  !=null) {
    		entity.setId(sourceDto.getId());
    		entity.setNaziv(sourceDto.getNaziv());
    		entity.setGodinaOsnivanja(sourceDto.getGodinaOsnivanja());
//            entity.setVina(vinoService.findOnebyId(sourceDto.getId()));
    		}
    			
    	
    	 return entity;
   
    }
}