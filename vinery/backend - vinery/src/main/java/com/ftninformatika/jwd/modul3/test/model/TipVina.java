package com.ftninformatika.jwd.modul3.test.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class TipVina {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	
	@Column(nullable = false)
	private String naziv;
	
	@OneToMany
	(mappedBy = "tipVina", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Vino> vina = new ArrayList<>();

	public TipVina() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipVina(Long id, String naziv, List<Vino> vina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.vina = vina;
	}

	@Override
	public String toString() {
		return "TipVina [id=" + id + ", naziv=" + naziv + ", vina=" + vina + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, naziv, vina);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipVina other = (TipVina) obj;
		return Objects.equals(id, other.id) && Objects.equals(naziv, other.naziv) && Objects.equals(vina, other.vina);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Vino> getVina() {
		return vina;
	}

	public void setVina(List<Vino> vina) {
		this.vina = vina;
	}
	
	


	

	}