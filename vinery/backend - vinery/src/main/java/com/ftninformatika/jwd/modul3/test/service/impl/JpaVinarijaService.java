package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.repository.VinarijaRepository;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;


@Service
@Transactional
public class JpaVinarijaService implements VinarijaService {
	
	@Autowired
	private VinarijaRepository	vinarijaRepository;

      @Override  
      public List<Vinarija> findAll() {
     return vinarijaRepository.findAll();
      }

	@Override
	public Vinarija findOne(Long id) {
		Optional<Vinarija> vinarija = vinarijaRepository.findById(id);
		if (vinarija.isPresent()) {
			return vinarija.get();
		}
		return null;
	}



	}



	