package com.ftninformatika.jwd.modul3.test.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Vino {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(nullable = false)
    private String naziv;
	
	@Column
    private String opis;
	
	@Column(nullable = false)
    private Integer godinaProizvodnje;
	
	@Column
    private Double cenaFlase;
	
	@Column
    private Integer dostupno;
	
	@ManyToOne
	private TipVina tipVina;
	
	@ManyToOne
	private Vinarija vinarija;

	public Vino() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vino(Long id, String naziv, String opis, Integer godinaProizvodnje, Double cenaFlase, Integer dostupno,
			TipVina tipVina, Vinarija vinarija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.godinaProizvodnje = godinaProizvodnje;
		this.cenaFlase = cenaFlase;
		this.dostupno = dostupno;
		this.tipVina = tipVina;
		this.vinarija = vinarija;
	}

	@Override
	public String toString() {
		return "Vino [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", godinaProizvodnje=" + godinaProizvodnje
				+ ", cenaFlase=" + cenaFlase + ", dostupno=" + dostupno + ", tipVina=" + tipVina + ", vinarija="
				+ vinarija + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(cenaFlase, dostupno, godinaProizvodnje, id, naziv, opis, tipVina, vinarija);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Integer getGodinaProizvodnje() {
		return godinaProizvodnje;
	}

	public void setGodinaProizvodnje(Integer godinaProizvodnje) {
		this.godinaProizvodnje = godinaProizvodnje;
	}

	public Double getCenaFlase() {
		return cenaFlase;
	}

	public void setCenaFlase(Double cenaFlase) {
		this.cenaFlase = cenaFlase;
	}

	public Integer getDostupno() {
		return dostupno;
	}

	public void setDostupno(Integer dostupno) {
		this.dostupno = dostupno;
	}

	public TipVina getTipVina() {
		return tipVina;
	}

	public void setTipVina(TipVina tipVina) {
		this.tipVina = tipVina;
	}

	public Vinarija getVinarija() {
		return vinarija;
	}

	public void setVinarija(Vinarija vinarija) {
		this.vinarija = vinarija;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vino other = (Vino) obj;
		return Objects.equals(cenaFlase, other.cenaFlase) && Objects.equals(dostupno, other.dostupno)
				&& Objects.equals(godinaProizvodnje, other.godinaProizvodnje) && Objects.equals(id, other.id)
				&& Objects.equals(naziv, other.naziv) && Objects.equals(opis, other.opis)
				&& Objects.equals(tipVina, other.tipVina) && Objects.equals(vinarija, other.vinarija);
	}

	


}