package com.ftninformatika.jwd.modul3.test.web.dto;


import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.ftninformatika.jwd.modul3.test.model.TipVina;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;

public class VinoDTO {

	 Long id;
	
	
    String naziv;
	
	
    String opis;
	
	
   Integer godinaProizvodnje;
	
	
    Double cenaFlase;
	
	
    Integer dostupno;
    
    String tipVina;
    
    String vinarijaNaziv;
	

//    @Positive
//	private Integer stanje;
//	
//    @NotNull
//	@Min(0)
//	private Double cena;
//	
//	@NotNull
//	@NotBlank
//	@Length(max = 15)
//	private String naziv;
//	

	Long tipVinaId;
	
	Long vinarijaId;

	public VinoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VinoDTO(Long id, String naziv, String opis, Integer godinaProizvodnje, Double cenaFlase, Integer dostupno,
			String tipVina, String vinarijaNaziv, Long tipVinaId, Long vinarijaId) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.godinaProizvodnje = godinaProizvodnje;
		this.cenaFlase = cenaFlase;
		this.dostupno = dostupno;
		this.tipVina = tipVina;
		this.vinarijaNaziv = vinarijaNaziv;
		this.tipVinaId = tipVinaId;
		this.vinarijaId = vinarijaId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(cenaFlase, dostupno, godinaProizvodnje, id, naziv, opis, tipVina, tipVinaId, vinarijaId,
				vinarijaNaziv);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VinoDTO other = (VinoDTO) obj;
		return Objects.equals(cenaFlase, other.cenaFlase) && Objects.equals(dostupno, other.dostupno)
				&& Objects.equals(godinaProizvodnje, other.godinaProizvodnje) && Objects.equals(id, other.id)
				&& Objects.equals(naziv, other.naziv) && Objects.equals(opis, other.opis)
				&& Objects.equals(tipVina, other.tipVina) && Objects.equals(tipVinaId, other.tipVinaId)
				&& Objects.equals(vinarijaId, other.vinarijaId) && Objects.equals(vinarijaNaziv, other.vinarijaNaziv);
	}

	@Override
	public String toString() {
		return "VinoDTO [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", godinaProizvodnje=" + godinaProizvodnje
				+ ", cenaFlase=" + cenaFlase + ", dostupno=" + dostupno + ", tipVina=" + tipVina + ", vinarijaNaziv="
				+ vinarijaNaziv + ", tipVinaId=" + tipVinaId + ", vinarijaId=" + vinarijaId + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Integer getGodinaProizvodnje() {
		return godinaProizvodnje;
	}

	public void setGodinaProizvodnje(Integer godinaProizvodnje) {
		this.godinaProizvodnje = godinaProizvodnje;
	}

	public Double getCenaFlase() {
		return cenaFlase;
	}

	public void setCenaFlase(Double cenaFlase) {
		this.cenaFlase = cenaFlase;
	}

	public Integer getDostupno() {
		return dostupno;
	}

	public void setDostupno(Integer dostupno) {
		this.dostupno = dostupno;
	}

	public String getTipVina() {
		return tipVina;
	}

	public void setTipVina(String tipVina) {
		this.tipVina = tipVina;
	}

	public String getVinarijaNaziv() {
		return vinarijaNaziv;
	}

	public void setVinarijaNaziv(String vinarijaNaziv) {
		this.vinarijaNaziv = vinarijaNaziv;
	}

	public Long getTipVinaId() {
		return tipVinaId;
	}

	public void setTipVinaId(Long tipVinaId) {
		this.tipVinaId = tipVinaId;
	}

	public Long getVinarijaId() {
		return vinarijaId;
	}

	public void setVinarijaId(Long vinarijaId) {
		this.vinarijaId = vinarijaId;
	}



	}