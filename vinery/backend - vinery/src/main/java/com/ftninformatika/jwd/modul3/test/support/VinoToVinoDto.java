package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.jaxb.SpringDataJaxb.SortDto;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.web.dto.VinoDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;

@Component
public class VinoToVinoDto implements Converter<Vino, VinoDTO> {

	@Autowired
	private VinarijaToVinarijaDto toVinarijaDto;
	
	@Autowired
	private TipVinaToTipVinaDto toTipVinaDto;
	
    @Override
    public VinoDTO convert(Vino source) {
    	VinoDTO dto = new VinoDTO();
    	
    	
    	dto.setId(source.getId());
    	dto.setOpis(source.getOpis());
    	dto.setGodinaProizvodnje(source.getGodinaProizvodnje());
    	dto.setCenaFlase(source.getCenaFlase());
    	dto.setDostupno(source.getDostupno());
    	dto.setNaziv(source.getNaziv());
    	
    	dto.setTipVinaId(source.getTipVina().getId());
    	dto.setTipVina(source.getTipVina().getNaziv());
        dto.setVinarijaId(source.getVinarija().getId());
        dto.setVinarijaNaziv(source.getVinarija().getNaziv());
    	
        return dto;
    }

    public List<VinoDTO> convert(List<Vino> vina){
        List<VinoDTO> vDto = new ArrayList<>();

        for(Vino v : vina) {
        	vDto.add(convert(v));
        }

        return vDto;
    }

}