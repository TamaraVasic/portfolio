package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;
import com.ftninformatika.jwd.modul3.test.service.VinoService;
import com.ftninformatika.jwd.modul3.test.service.TipVinaService;
import com.ftninformatika.jwd.modul3.test.service.VinarijaService;
import com.ftninformatika.jwd.modul3.test.web.dto.VinoDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.VinarijaDTO;


@Component
public class VinoDtoToVino implements Converter<VinoDTO, Vino> {

    @Autowired
    private VinoService vinoService;
    
    @Autowired 
    private TipVinaService tipVinaService;
    
    @Autowired
    private VinarijaService vinarijaService;

    @Override
    public Vino convert(VinoDTO sourceDto) {
    	
    	Vino entity;
    	
    	if(sourceDto.getId() == null) {
            entity = new Vino() ;
		}
    	else {
			entity = vinoService.findOnebyId(sourceDto.getId());
		}
    	
 
            entity.setNaziv(sourceDto.getNaziv());
            entity.setOpis(sourceDto.getOpis());
            entity.setGodinaProizvodnje(sourceDto.getGodinaProizvodnje());
            entity.setCenaFlase(sourceDto.getCenaFlase());
            entity.setDostupno(sourceDto.getDostupno());
            entity.setTipVina(tipVinaService.findOnebyID(sourceDto.getTipVinaId()));
            entity.setVinarija(vinarijaService.findOne(sourceDto.getVinarijaId()));
            
      
    			
    	
    	 return entity;
   
    }
}