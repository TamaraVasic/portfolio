package com.ftninformatika.jwd.modul3.test.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


	@Entity
	public class Vinarija {

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

		@Column(nullable = false)
	    private String naziv;
		
		@Column
	    private Integer godinaOsnivanja;
		
		@OneToMany
		(mappedBy = "vinarija", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	   private List<Vino> vina = new ArrayList<>();

		public Vinarija() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Vinarija(Long id, String naziv, Integer godinaOsnivanja, List<Vino> vina) {
			super();
			this.id = id;
			this.naziv = naziv;
			this.godinaOsnivanja = godinaOsnivanja;
			this.vina = vina;
		}

		@Override
		public String toString() {
			return "Vinarija [id=" + id + ", naziv=" + naziv + ", godinaOsnivanja=" + godinaOsnivanja + ", vina=" + vina
					+ "]";
		}

		@Override
		public int hashCode() {
			return Objects.hash(godinaOsnivanja, id, naziv, vina);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Vinarija other = (Vinarija) obj;
			return Objects.equals(godinaOsnivanja, other.godinaOsnivanja) && Objects.equals(id, other.id)
					&& Objects.equals(naziv, other.naziv) && Objects.equals(vina, other.vina);
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getNaziv() {
			return naziv;
		}

		public void setNaziv(String naziv) {
			this.naziv = naziv;
		}

		public Integer getGodinaOsnivanja() {
			return godinaOsnivanja;
		}

		public void setGodinaOsnivanja(Integer godinaOsnivanja) {
			this.godinaOsnivanja = godinaOsnivanja;
		}

		public List<Vino> getVina() {
			return vina;
		}

		public void setVina(List<Vino> vina) {
			this.vina = vina;
		}

	}
		