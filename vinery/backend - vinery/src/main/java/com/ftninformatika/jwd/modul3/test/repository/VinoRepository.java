package com.ftninformatika.jwd.modul3.test.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.test.model.Vino;
import com.ftninformatika.jwd.modul3.test.model.Vinarija;


@Repository
public interface VinoRepository extends JpaRepository<Vino,Long> {



	Page<Vino> findByNazivIgnoreCaseContains(String nazivVina, Pageable pageNo);

	Page<Vino> findByVinarijaIdAndNazivIgnoreCaseContains(Long vinarijaId,String nazivVina,Pageable pageNo);


}


