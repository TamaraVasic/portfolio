INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
      
INSERT INTO buyers (name_and_surname, adress, phone, max_price) VALUES ('Petar Petrović', 'Bulevar Evrope 35', 0605506894, 100000);
INSERT INTO buyers (name_and_surname, adress, phone, max_price) VALUES ('Jovan Jovanović', 'Kosovska 2', 0605506444, 55000);
INSERT INTO buyers (name_and_surname, adress, phone, max_price) VALUES ('Ana Maksimović', 'Patrizanska 44', 060552394, 30000);
              
INSERT INTO sellers (name_and_surname, adress, phone) VALUES ('Marija Petrović', 'Bulevar Evrope 89', 060552394);
INSERT INTO sellers (name_and_surname, adress, phone) VALUES ('Marija Mitrović', 'Dalmatinska 89', 060552394);
INSERT INTO sellers (name_and_surname, adress, phone) VALUES ('Lazar Krstić', 'Subotička 89', 060552394);

INSERT INTO state (state_type) VALUES ('AVAILABLE');
INSERT INTO state (state_type) VALUES ('RESERVED');
INSERT INTO state (state_type) VALUES ('SIGNING');

INSERT INTO real_estate (type, description, size, price, adress, city, sellers_id, state_id) 
			VALUES ('stan', 'Novogradnja, petosoban stan na I spratu sa odličnim pogledom',100,180000,'Subotička 89', 'Novi Sad', 1, 1);
INSERT INTO real_estate (type, description, size, price, adress, city, sellers_id, state_id) 
			VALUES ('lokal', 'Novogradnja, ulični lokal na uglu',150,300000,'Subotička 89', 'Novi Sad', 3, 1);
INSERT INTO real_estate (type, description, size, price, adress, city, sellers_id, state_id) 
			VALUES ('garaza', 'U suterenu zgrade sa automatskim vratima',25,30000,'Subotička 89', 'Novi Sad', 2, 2);