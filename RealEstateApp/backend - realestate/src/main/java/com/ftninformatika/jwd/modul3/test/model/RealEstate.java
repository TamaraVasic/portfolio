package com.ftninformatika.jwd.modul3.test.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class RealEstate {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(nullable = false)
    private String type;
	
	@Column
    private String description;
	
	@Column
    private Double size;
	
	@Column
    private Double price;
	
	@Column
    private String adress;
	
	@Column
    private String city;
	
	@ManyToOne
	private Sellers sellers;

	@ManyToOne
	private State state;

	public RealEstate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RealEstate(Long id, String type, String description, Double size, Double price, String adress, String city,
			Sellers sellers, State state) {
		super();
		this.id = id;
		this.type = type;
		this.description = description;
		this.size = size;
		this.price = price;
		this.adress = adress;
		this.city = city;
		this.sellers = sellers;
		this.state = state;
	}

	@Override
	public String toString() {
		return "RealEstate [id=" + id + ", type=" + type + ", description=" + description + ", size=" + size
				+ ", price=" + price + ", adress=" + adress + ", city=" + city + ", sellers=" + sellers + ", state="
				+ state + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adress, city, description, id, price, sellers, size, state, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RealEstate other = (RealEstate) obj;
		return Objects.equals(adress, other.adress) && Objects.equals(city, other.city)
				&& Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(price, other.price) && Objects.equals(sellers, other.sellers)
				&& Objects.equals(size, other.size) && Objects.equals(state, other.state)
				&& Objects.equals(type, other.type);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Sellers getSellers() {
		return sellers;
	}

	public void setSellers(Sellers sellers) {
		this.sellers = sellers;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	

	}