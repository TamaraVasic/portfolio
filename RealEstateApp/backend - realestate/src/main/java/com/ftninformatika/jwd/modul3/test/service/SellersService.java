package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;



public interface SellersService {
	


	List<Sellers> findAll();

	Sellers findOne(Long id);



	
	
}