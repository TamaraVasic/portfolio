package com.ftninformatika.jwd.modul3.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.service.RealEstateService;
import com.ftninformatika.jwd.modul3.test.support.RealEstateDtoToRealEstate;
import com.ftninformatika.jwd.modul3.test.support.RealEstateToRealEstateDto;
import com.ftninformatika.jwd.modul3.test.web.dto.RealEstateDTO;

@RestController
@RequestMapping(value="/api/realestate")
public class RealEstateController {
	
	@Autowired
	private RealEstateService realEstateService;
	
	@Autowired
	private RealEstateToRealEstateDto toRealEstateDto;
	
	@Autowired
	private RealEstateDtoToRealEstate toRealEstate;
	
    @PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping ("/{id}")
	public ResponseEntity<RealEstateDTO> getOne(@PathVariable Long id) {
		System.out.println(id);
		RealEstate vino = realEstateService.findOnebyId(id);
		if (vino == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		else {
		return new ResponseEntity<>(toRealEstateDto.convert(vino), HttpStatus.OK);
	}
	}
	 
    @PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<RealEstateDTO>> getAll(	         
	            @RequestParam(required=false) Long size,
	            @RequestParam(required=false) String type,
	            @RequestParam(required=false,value = "pageNo", defaultValue = "0") int pageNo){
		   
	        Page<RealEstate> page = realEstateService.find(size,type,pageNo);

	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

	        return new ResponseEntity<List<RealEstateDTO>>(toRealEstateDto.convert(page.getContent()),headers, HttpStatus.OK);
	    }
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<RealEstateDTO> create(@Valid @RequestBody RealEstateDTO realEstateDTO) {	
			RealEstate savedRealEstate = realEstateService.save(toRealEstate.convert(realEstateDTO));
			return new ResponseEntity<RealEstateDTO>(toRealEstateDto.convert(savedRealEstate), HttpStatus.CREATED);
		}
	 
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value="/{id}", consumes=MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<RealEstateDTO> update(@PathVariable Long id, @Valid @RequestBody RealEstateDTO realEstateDTO){
	        if(!id.equals(realEstateDTO.getId())) {
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	        RealEstate realEstate = toRealEstate.convert(realEstateDTO);
	        RealEstate savedRealEstate = realEstateService.update(realEstate);
	        
	        return new ResponseEntity<>(toRealEstateDto.convert(savedRealEstate),HttpStatus.OK);
	  }
		
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}") 		
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		RealEstate deletedRealEstate = realEstateService.delete(id);
			if (deletedRealEstate!=null) {			
			return new ResponseEntity<> (HttpStatus.NO_CONTENT);			
			} else {						
			return new ResponseEntity<> (HttpStatus.NOT_FOUND);
			}
	  }
}

