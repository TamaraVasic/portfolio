package com.ftninformatika.jwd.modul3.test.web.dto;


import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;

public class RealEstateDTO {

	 Long id;
	
	
    String type;
	
	
    Double size;
	
    @NotNull
    @Min(0)
    Double price;
    
    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	String description;
	
	
    String adress;
	
	
    String city;
    
    String sellerNameAndSurname;
    
    String stateType;
	

//    @Positive
//	private Integer stanje;
//	
//    @NotNull
//	@Min(0)
//	private Double cena;
//	
//	@NotNull
//	@NotBlank
//	@Length(max = 15)
//	private String naziv;
//	

	Long sellerId;
	
	Long stateId;

	public RealEstateDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public RealEstateDTO(Long id, String type, Double size, @NotNull @Min(0) Double price, String description,
			String adress, String city, String sellerNameAndSurname, String stateType, Long sellerId, Long stateId) {
		super();
		this.id = id;
		this.type = type;
		this.size = size;
		this.price = price;
		this.description = description;
		this.adress = adress;
		this.city = city;
		this.sellerNameAndSurname = sellerNameAndSurname;
		this.stateType = stateType;
		this.sellerId = sellerId;
		this.stateId = stateId;
	}


	@Override
	public String toString() {
		return "RealEstateDTO [id=" + id + ", type=" + type + ", size=" + size + ", price=" + price + ", description="
				+ description + ", adress=" + adress + ", city=" + city + ", sellerNameAndSurname="
				+ sellerNameAndSurname + ", stateType=" + stateType + ", sellerId=" + sellerId + ", stateId=" + stateId
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adress, city, description, id, price, sellerId, sellerNameAndSurname, size, stateId,
				stateType, type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RealEstateDTO other = (RealEstateDTO) obj;
		return Objects.equals(adress, other.adress) && Objects.equals(city, other.city)
				&& Objects.equals(description, other.description) && Objects.equals(id, other.id)
				&& Objects.equals(price, other.price) && Objects.equals(sellerId, other.sellerId)
				&& Objects.equals(sellerNameAndSurname, other.sellerNameAndSurname) && Objects.equals(size, other.size)
				&& Objects.equals(stateId, other.stateId) && Objects.equals(stateType, other.stateType)
				&& Objects.equals(type, other.type);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getSize() {
		return size;
	}

	public void setSize(Double size) {
		this.size = size;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSellerNameAndSurname() {
		return sellerNameAndSurname;
	}

	public void setSellerNameAndSurname(String sellerNameAndSurname) {
		this.sellerNameAndSurname = sellerNameAndSurname;
	}

	public String getStateType() {
		return stateType;
	}

	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	
}