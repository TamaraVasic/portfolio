package com.ftninformatika.jwd.modul3.test.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


	@Entity
	public class Sellers {

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

		@Column(nullable = false)
	    private String nameAndSurname;
		
		@Column
	    private String adress;
		
		@Column
	    private Integer phone;
		
	
		@OneToMany
		(mappedBy = "sellers", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	   private List<RealEstate> realEstates = new ArrayList<>();


		public Sellers() {
			super();
			// TODO Auto-generated constructor stub
		}


		public Sellers(Long id, String nameAndSurname, String adress, Integer phone, List<RealEstate> realEstates) {
			super();
			this.id = id;
			this.nameAndSurname = nameAndSurname;
			this.adress = adress;
			this.phone = phone;
			this.realEstates = realEstates;
		}


		@Override
		public String toString() {
			return "Sellers [id=" + id + ", nameAndSurname=" + nameAndSurname + ", adress=" + adress + ", phone="
					+ phone + ", realEstates=" + realEstates + "]";
		}


		@Override
		public int hashCode() {
			return Objects.hash(adress, id, nameAndSurname, phone, realEstates);
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Sellers other = (Sellers) obj;
			return Objects.equals(adress, other.adress) && Objects.equals(id, other.id)
					&& Objects.equals(nameAndSurname, other.nameAndSurname) && Objects.equals(phone, other.phone)
					&& Objects.equals(realEstates, other.realEstates);
		}


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getNameAndSurname() {
			return nameAndSurname;
		}


		public void setNameAndSurname(String nameAndSurname) {
			this.nameAndSurname = nameAndSurname;
		}


		public String getAdress() {
			return adress;
		}


		public void setAdress(String adress) {
			this.adress = adress;
		}


		public Integer getPhone() {
			return phone;
		}


		public void setPhone(Integer phone) {
			this.phone = phone;
		}


		public List<RealEstate> getRealEstates() {
			return realEstates;
		}


		public void setRealEstates(List<RealEstate> realEstates) {
			this.realEstates = realEstates;
		}
		
		

	}	