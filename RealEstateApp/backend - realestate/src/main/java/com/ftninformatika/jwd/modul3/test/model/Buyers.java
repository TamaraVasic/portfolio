package com.ftninformatika.jwd.modul3.test.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Buyers {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	
	@Column(nullable = false)
    private String nameAndSurname;
	
	@Column
    private String adress;
	
	@Column
    private Integer phone;
	
	@Column
    private Integer maxPrice;

	public Buyers() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Buyers(Long id, String nameAndSurname, String adress, Integer phone, Integer maxPrice) {
		super();
		this.id = id;
		this.nameAndSurname = nameAndSurname;
		this.adress = adress;
		this.phone = phone;
		this.maxPrice = maxPrice;
	}

	@Override
	public String toString() {
		return "Buyers [id=" + id + ", nameAndSurname=" + nameAndSurname + ", adress=" + adress + ", phone=" + phone
				+ ", maxPrice=" + maxPrice + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adress, id, maxPrice, nameAndSurname, phone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Buyers other = (Buyers) obj;
		return Objects.equals(adress, other.adress) && Objects.equals(id, other.id)
				&& Objects.equals(maxPrice, other.maxPrice) && Objects.equals(nameAndSurname, other.nameAndSurname)
				&& Objects.equals(phone, other.phone);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameAndSurname() {
		return nameAndSurname;
	}

	public void setNameAndSurname(String nameAndSurname) {
		this.nameAndSurname = nameAndSurname;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public Integer getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	
	
		}