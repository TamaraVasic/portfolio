package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.service.SellersService;
import com.ftninformatika.jwd.modul3.test.service.RealEstateService;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;


@Component
public class SellersDtoToSellers implements Converter<SellersDTO, Sellers> {

    @Autowired
    private SellersService sellersService;
    
//    @Autowired
//    private VinoService vinoService;

    @Override
    public Sellers convert(SellersDTO sourceDto) {
    	
    	Sellers entity = null;
    	
    	if(sourceDto.getId() == null) {
    		entity = new Sellers() ;
		}
    	else {
    		entity = sellersService.findOne(sourceDto.getId());
		}
    	
    	if ( entity  !=null) {
    		entity.setId(sourceDto.getId());
    		entity.setNameAndSurname(sourceDto.getNameAndSurname());
//    		entity.setGodinaOsnivanja(sourceDto.getGodinaOsnivanja());
//            entity.setVina(vinoService.findOnebyId(sourceDto.getId()));
    		}
    			
    	
    	 return entity;
   
    }
}