package com.ftninformatika.jwd.modul3.test.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.model.Sellers;


@Repository
public interface RealEstateRepository extends JpaRepository<RealEstate,Long> {



	Page<RealEstate> findByTypeIgnoreCaseContains(String type, Pageable pageNo);

   Page<RealEstate> findBySizeAndTypeIgnoreCaseContains(Long size,String type,Pageable pageNo);


}


