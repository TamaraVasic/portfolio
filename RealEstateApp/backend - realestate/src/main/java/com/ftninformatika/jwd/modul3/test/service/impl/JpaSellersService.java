package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.repository.SellersRepository;
import com.ftninformatika.jwd.modul3.test.service.SellersService;


@Service
@Transactional
public class JpaSellersService implements SellersService {
	
	@Autowired
	private SellersRepository	sellersRepository;

      @Override  
      public List<Sellers> findAll() {
     return sellersRepository.findAll();
      }

	@Override
	public Sellers findOne(Long id) {
		Optional<Sellers> sellers = sellersRepository.findById(id);
		if (sellers.isPresent()) {
			return sellers.get();
		}
		return null;
	}



	}



	