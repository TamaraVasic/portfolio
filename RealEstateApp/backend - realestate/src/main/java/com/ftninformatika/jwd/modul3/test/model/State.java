package com.ftninformatika.jwd.modul3.test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class State {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(nullable = false)
    private String stateType;
	
	@OneToMany
	(mappedBy = "state", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	   private List<RealEstate> realEs = new ArrayList<>();
	

	public State() {
		super();
		// TODO Auto-generated constructor stub
	}

	public State(Long id, String type, RealEstate realEstates) {
		super();
		this.id = id;
		this.stateType = type;
		this.realEs = (List<RealEstate>) realEstates;
	}

	@Override
	public String toString() {
		return "State [id=" + id + ", stateType=" + stateType + ", realEs=" + realEs + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, realEs, stateType);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		return Objects.equals(id, other.id) && Objects.equals(realEs, other.realEs)
				&& Objects.equals(stateType, other.stateType);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStateType() {
		return stateType;
	}

	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	public RealEstate getRealEs() {
		return (RealEstate) realEs;
	}

	public void setRealEs(RealEstate realEs) {
		this.realEs = (List<RealEstate>) realEs;
	}
	
	
}
