package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;

@Component
public class SellersToSellersDto implements Converter<Sellers, SellersDTO> {

    @Override
    public SellersDTO convert(Sellers source) {
    	SellersDTO dto = new SellersDTO();
    	dto.setId(source.getId());
    	dto.setNameAndSurname(source.getNameAndSurname());
//    	dto.setGodinaOsnivanja(source.getGodinaOsnivanja());
//    	dto.setProizvodId(source.getProizvod().getId());
     
        return dto;
    }

    public List<SellersDTO> convert(List<Sellers> sellers){
        List<SellersDTO> sDto = new ArrayList<>();

        for(Sellers s : sellers) {
        	sDto.add(convert(s));
        }

        return sDto;
    }

}