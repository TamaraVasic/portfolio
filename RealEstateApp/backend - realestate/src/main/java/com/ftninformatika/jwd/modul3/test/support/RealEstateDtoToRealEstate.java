package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.service.RealEstateService;
import com.ftninformatika.jwd.modul3.test.service.ByersService;
import com.ftninformatika.jwd.modul3.test.service.SellersService;
import com.ftninformatika.jwd.modul3.test.web.dto.RealEstateDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;


@Component
public class RealEstateDtoToRealEstate implements Converter<RealEstateDTO, RealEstate> {

    @Autowired
    private RealEstateService realEstateService;
    
//    @Autowired 
//    private ByersService tipVinaService;
//    
//    @Autowired
//    private SellersService vinarijaService;

    @Override
    public RealEstate convert(RealEstateDTO sourceDto) {
    	
    	RealEstate entity;
    	
    	if(sourceDto.getId() == null) {
            entity = new RealEstate() ;
		}
    	else {
			entity = realEstateService.findOnebyId(sourceDto.getId());
		}
    	
 
//            entity.setNaziv(sourceDto.getNaziv());
//            entity.setOpis(sourceDto.getOpis());
//            entity.setGodinaProizvodnje(sourceDto.getGodinaProizvodnje());
//            entity.setCenaFlase(sourceDto.getCenaFlase());
//            entity.setDostupno(sourceDto.getDostupno());
//            entity.setTipVina(tipVinaService.findOnebyID(sourceDto.getTipVinaId()));
//            entity.setVinarija(vinarijaService.findOne(sourceDto.getVinarijaId()));
            
      
    			
    	
    	 return entity;
   
    }
}