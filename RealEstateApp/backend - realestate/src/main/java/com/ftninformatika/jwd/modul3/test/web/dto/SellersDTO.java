package com.ftninformatika.jwd.modul3.test.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.*;

public class SellersDTO {

    //@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;

//    @NotBlank(message = "Naziv prevoznika nije zadat.")
    private String nameAndSurname;
    
    private String adress;
    
   
    private Integer phone;

	List<RealEstateDTO> realEstates;

	public SellersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SellersDTO(Long id, String nameAndSurname, String adress, Integer phone, List<RealEstateDTO> realEstates) {
		super();
		this.id = id;
		this.nameAndSurname = nameAndSurname;
		this.adress = adress;
		this.phone = phone;
		this.realEstates = realEstates;
	}

	@Override
	public String toString() {
		return "SellersDTO [id=" + id + ", nameAndSurname=" + nameAndSurname + ", adress=" + adress + ", phone=" + phone
				+ ", realEstates=" + realEstates + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adress, id, nameAndSurname, phone, realEstates);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SellersDTO other = (SellersDTO) obj;
		return Objects.equals(adress, other.adress) && Objects.equals(id, other.id)
				&& Objects.equals(nameAndSurname, other.nameAndSurname) && Objects.equals(phone, other.phone)
				&& Objects.equals(realEstates, other.realEstates);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameAndSurname() {
		return nameAndSurname;
	}

	public void setNameAndSurname(String nameAndSurname) {
		this.nameAndSurname = nameAndSurname;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public List<RealEstateDTO> getRealEstates() {
		return realEstates;
	}

	public void setRealEstates(List<RealEstateDTO> realEstates) {
		this.realEstates = realEstates;
	}

	
	
}