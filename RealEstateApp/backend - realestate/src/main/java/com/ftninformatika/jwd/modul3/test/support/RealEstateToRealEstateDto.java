package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.jaxb.SpringDataJaxb.SortDto;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.web.dto.RealEstateDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;

@Component
public class RealEstateToRealEstateDto implements Converter<RealEstate, RealEstateDTO> {

	@Autowired
	private SellersToSellersDto toSellersDto;
	
//	@Autowired
//	private ByersToByersDto toTipVinaDto;
	
    @Override
    public RealEstateDTO convert(RealEstate source) {
    	RealEstateDTO dto = new RealEstateDTO();
    	
    	
    	dto.setId(source.getId());
     	dto.setType(source.getType());
    	dto.setSize(source.getSize());
    	dto.setDescription(source.getDescription());
    	dto.setPrice(source.getPrice());
    	dto.setAdress(source.getAdress());
    	dto.setCity(source.getCity());
    	
 dto.setSellerId(source.getSellers().getId());
dto.setSellerNameAndSurname(source.getSellers().getNameAndSurname());
//        dto.setVinarijaId(source.getVinarija().getId());
//        dto.setVinarijaNaziv(source.getVinarija().getNaziv());
    	
        return dto;
    }

    public List<RealEstateDTO> convert(List<RealEstate> realestates){
        List<RealEstateDTO> rDto = new ArrayList<>();

        for(RealEstate r : realestates) {
        	rDto.add(convert(r));
        }

        return rDto;
    }

}