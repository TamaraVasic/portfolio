package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.Jsr310Converters.ZoneIdToStringConverter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.service.ByersService;
import com.ftninformatika.jwd.modul3.test.service.SellersService;
import com.ftninformatika.jwd.modul3.test.web.dto.BuyersDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;


@Component
public class ByersDtoToByers implements Converter<BuyersDTO, Buyers> {

    @Autowired
    private ByersService kategorijaService;

    @Override
    public Buyers convert(BuyersDTO sourceDto) {
    	
    	Buyers entity = null;
    	
    	if(sourceDto.getId() == null) {
            entity = new Buyers() ;
		}
    	else {
    		entity = kategorijaService.findOnebyID(sourceDto.getId());
		}
    	
    	if ( entity  !=null) {
    		entity.setId(sourceDto.getId());
    		entity.setNameAndSurname(sourceDto.getNameAndSurname());
   
    }
		return entity;
}
}