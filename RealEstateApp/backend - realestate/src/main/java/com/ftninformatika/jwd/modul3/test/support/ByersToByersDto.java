package com.ftninformatika.jwd.modul3.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.web.dto.BuyersDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;

@Component
public class ByersToByersDto implements Converter<Buyers, BuyersDTO> {

    @Override
    public BuyersDTO convert(Buyers source) {
    	BuyersDTO dto = new BuyersDTO();
    	dto.setId(source.getId());
    	dto.setNameAndSurname(source.getNameAndSurname());

     
        return dto;
    }

    public List<BuyersDTO> convert(List<Buyers> byers){
        List<BuyersDTO> bDto = new ArrayList<>();

        for(Buyers b : byers) {
        	bDto.add(convert(b));
        }

        return bDto;
    }

}