package com.ftninformatika.jwd.modul3.test.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;

import java.util.*;

public class BuyersDTO {

    //@Positive(message = "Id mora biti pozitivan broj.")
    private Long id;

//    @NotBlank(message = "Naziv kategorije nije zadat.")
    private String nameAndSurname;
    
    private String adress;
    
    private Integer phone;
    
    private Integer maxPrice;

	public BuyersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BuyersDTO(Long id, String nameAndSurname, String adress, Integer phone, Integer maxPrice) {
		super();
		this.id = id;
		this.nameAndSurname = nameAndSurname;
		this.adress = adress;
		this.phone = phone;
		this.maxPrice = maxPrice;
	}

	@Override
	public String toString() {
		return "BuyersDTO [id=" + id + ", nameAndSurname=" + nameAndSurname + ", adress=" + adress + ", phone=" + phone
				+ ", maxPrice=" + maxPrice + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(adress, id, maxPrice, nameAndSurname, phone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuyersDTO other = (BuyersDTO) obj;
		return Objects.equals(adress, other.adress) && Objects.equals(id, other.id)
				&& Objects.equals(maxPrice, other.maxPrice) && Objects.equals(nameAndSurname, other.nameAndSurname)
				&& Objects.equals(phone, other.phone);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameAndSurname() {
		return nameAndSurname;
	}

	public void setNameAndSurname(String nameAndSurname) {
		this.nameAndSurname = nameAndSurname;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public Integer getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Integer maxPrice) {
		this.maxPrice = maxPrice;
	}

	
}
   
    
    
    