package com.ftninformatika.jwd.modul3.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.service.impl.JpaRealEstateService;



public interface RealEstateService {
	
	RealEstate findOnebyId(Long id);
	
	RealEstate save(RealEstate realEstate);
	
	RealEstate update(RealEstate realEstate);
	
	RealEstate delete(Long id);
	
Page<RealEstate> find(Long size, String type, int pageNo);

	
}