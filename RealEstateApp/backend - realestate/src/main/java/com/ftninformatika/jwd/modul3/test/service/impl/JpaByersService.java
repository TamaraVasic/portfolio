package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.repository.BuyersRepository;
import com.ftninformatika.jwd.modul3.test.repository.SellersRepository;
import com.ftninformatika.jwd.modul3.test.service.ByersService;
import com.ftninformatika.jwd.modul3.test.service.SellersService;


@Service
@Transactional
public class JpaByersService implements ByersService {
	
	@Autowired
	private BuyersRepository byersRepository;


	@Override
	public List<Buyers> findAll() {
		// TODO Auto-generated method stub
		return byersRepository.findAll();
	}

	@Override
	public Buyers findOnebyID(Long id) {
		Optional<Buyers> byers = byersRepository.findById(id);

		if (byers.isPresent()) {
			return byers.get();
		}

		return null;
	}

	}



	