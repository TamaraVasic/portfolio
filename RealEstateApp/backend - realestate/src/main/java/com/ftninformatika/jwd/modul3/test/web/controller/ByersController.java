package com.ftninformatika.jwd.modul3.test.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.ftninformatika.jwd.modul3.test.model.Buyers;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.service.ByersService;
import com.ftninformatika.jwd.modul3.test.service.SellersService;
import com.ftninformatika.jwd.modul3.test.support.ByersDtoToByers;
import com.ftninformatika.jwd.modul3.test.support.ByersToByersDto;
import com.ftninformatika.jwd.modul3.test.support.SellersDtoToSellers;
import com.ftninformatika.jwd.modul3.test.support.SellersToSellersDto;
import com.ftninformatika.jwd.modul3.test.web.dto.BuyersDTO;
import com.ftninformatika.jwd.modul3.test.web.dto.SellersDTO;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/byers",produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ByersController {
	
	@Autowired
	private ByersService byersService;
	
	@Autowired
	private ByersToByersDto toByersDto;
	
	@Autowired
	private ByersDtoToByers toByers;
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public	ResponseEntity<List<BuyersDTO>>getAll() {
		List<Buyers> byersList = byersService.findAll();
		return new ResponseEntity<>(toByersDto.convert(byersList),HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping(value = "/{id}")
	public ResponseEntity<BuyersDTO> getOne(@PathVariable Long id){
		Buyers byers = byersService.findOnebyID(id);
			return new ResponseEntity<>(toByersDto.convert(byers),HttpStatus.OK);
		}

	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
	    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	
}
	

   