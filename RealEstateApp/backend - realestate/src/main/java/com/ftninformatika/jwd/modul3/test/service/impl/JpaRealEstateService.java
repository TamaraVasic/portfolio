package com.ftninformatika.jwd.modul3.test.service.impl;

import java.util.List;
import java.util.Optional;

import javax.persistence.Id;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.modul3.test.model.RealEstate;
import com.ftninformatika.jwd.modul3.test.model.Sellers;
import com.ftninformatika.jwd.modul3.test.repository.RealEstateRepository;
import com.ftninformatika.jwd.modul3.test.repository.SellersRepository;
import com.ftninformatika.jwd.modul3.test.service.RealEstateService;
import com.ftninformatika.jwd.modul3.test.service.SellersService;


@Service
@Transactional
public class JpaRealEstateService implements RealEstateService {
	@Autowired
	private RealEstateRepository	realEstateRepository;



	@Override
	public RealEstate save(RealEstate vino) {
		// TODO Auto-generated method stub
		return realEstateRepository.save(vino);
	}

	@Override
	public RealEstate findOnebyId(Long id) {
		Optional<RealEstate> realEstate = realEstateRepository.findById(id);
		if (realEstate.isPresent()) {
			return realEstate.get();
		}
		return null;
	}



	@Override
	public RealEstate delete(Long id) {
	
     Optional<RealEstate> realEstate = realEstateRepository.findById(id);
		
		if(realEstate.isPresent()) {
			
			realEstateRepository.deleteById(id);
				
		return realEstate.get();
	}
		return null;
	}


	@Override
	public Page<RealEstate> find(Long size, String type, int pageNo) {
		if (type == null) {
			type = "";
		}
		
		if (size == null) {
			return realEstateRepository.findByTypeIgnoreCaseContains( type, PageRequest.of(pageNo, 3));
		}
		
      return realEstateRepository.findBySizeAndTypeIgnoreCaseContains(size, type, PageRequest.of(pageNo, 3));
		
	}


	@Override
	public RealEstate update(RealEstate realEstate) {
		// TODO Auto-generated method stub
		return realEstateRepository.save(realEstate);
	}

}

	