import React from "react";
import { Table, Button, Form, ButtonGroup,Row, Col } from "react-bootstrap";
import { withParams, withNavigation } from "../../routeconf";
import AppAxios from "../../apis/AppAxios";

class RealEstates extends React.Component {
  constructor(props) {
    super(props);
//ovde definises pocetne parametre za search koji su ti potrebni
   const search = {
      size: "",
      type: "",
      pageNo: 0,
    };

 
//u state pozivas pocetne vrednosti create, search koje si gore definisala i mapu linija i prevoznika koje ces dobiti iz baze
    this.state = {
      realEstates: [],
      sellers: [],
      states: [],
      pageNo: 0,
      totalPages: 2,
      search: search
    };
  }

 
  componentDidMount() {
    this.getData();
  }

  async getData() {
    await this.getRealEstates(0);
    await this.getSellers();
    await this.getBuyers()
  }

  async getRealEstates(newPageNo) {
      let config = { 
      params: {
        id: this.state.search.id,
    
        type: this.state.search.type,
        pageNo: newPageNo,
    } 
  };

    AppAxios.get('/realestate', config)
    .then(res => {
        console.log(res);
        this.setState({
            realEstates: res.data,
            pageNo: newPageNo,
            totalPages : res.headers['total-pages']
        });
    })
    .catch(error => {
        // handle error
        console.log(error);
        alert('Error occured please try again!');
    });
}


getSellers(){
  AppAxios.get("/sellers")
  .then((response)=>{
      this.setState({sellers:response.data});
  })
  .catch((err=>{console.log(err)}));
}

getBuyers(){
    AppAxios.get("/buyers")
    .then((response)=>{
        this.setState({buyers:response.data});
    })
    .catch((err=>{console.log(err)}));
  }

deleteFromState(id) {
  var realEstates = this.state.realEstates;
  realEstates.forEach((element, index) => {
      if (element.id === id) {
        realEstates.splice(index, 1);
          this.setState({ realEstates: realEstates });
      }
  });
}

delete(id) {
  AppAxios.delete('/realestate/' + id)
      .then(res => {
          // handle success
          console.log(res);
          alert('Product was deleted successfully!');
          this.deleteFromState(id); // ili refresh page-a window.location.reload();
      })
      .catch(error => {
          // handle error
          console.log(error);
          alert('Error occured please try again!');
      });
}


onInputChange(event) {
  const name = event.target.name;
  const value = event.target.value

  let search = this.state.search;
  search[name] = value;

  this.setState({ search })
}

goToAdd() {
  this.props.navigate("/realestates/add");
}

onNumberChange(event) {
  console.log(event.target.value);

  const { name, value } = event.target;
  console.log(name + ", " + value);

  this.setState((state, props) => ({
      number: value}));
}


renderRealEstates() {
  return this.state.realEstates.map((realEstates) => {

      let realEstateId = realEstates.id;

      return (
          <tr key={realEstateId}>
              <td>{realEstates.id}</td>
              <td>{realEstates.type}</td>
              <td>{realEstates.description}</td>
              <td>{realEstates.size}</td>
              <td>{realEstates.price}</td>
              <td>{realEstates.adress}</td>
              <td>{realEstates.city}</td>
          </tr>
      )
  })
}

renderSearchForm() {
  return (
      <>
      <Form style={{ width: "100%" }}>
          <Row><Col>
              <Form.Group>
                  <Form.Label>Površina</Form.Label>
                  <Form.Control
                      name="size"
                      as="input"
                      type="number"
                      onChange={(e) => this.onInputChange(e)}></Form.Control>
              </Form.Group>
          </Col></Row>

          <Row><Col>
              <Form.Group>
                  <Form.Label>Tip nekretnine</Form.Label>
                  <Form.Control name="realEstateId" 
                  as="select"
                  type="number"
                  onChange={(e)=>this.onInputChange(e)}>
                      <option value=""></option>
                      {this.state.realEstates.map((realEstates)=>{
                          return(
                              <option value={realEstates.id}>{realEstates.type}</option>
                          );
                      })}
                  </Form.Control>
              </Form.Group>
          </Col></Row>
      </Form>
      <Row><Col>
          <Button className="mt-3" onClick={() => this.getRealEstates()}>Pretrazi</Button>
      </Col></Row>
      </>
  );
}
render() {
  return (
      <Col>
          <Row><h1>Nekretnine</h1></Row>

          <Row>
              {this.renderSearchForm()}
          </Row>
          <br/>

          {window.localStorage['role']=='ROLE_ADMIN'?
          <Row><Col>
          <Button onClick={() => this.goToAdd() }>Dodaj</Button>
          </Col></Row>: null}

          <Row><Col>
          <Table style={{ width: "100%" }}>
          <thead>
          <tr>
          <th>Id</th>
          <th>Tip</th>
          <th>Opis</th>
          <th>Površina</th>
          <th>Cena</th>
          <th>Adresa</th>
          <th>Grad</th>
          <th></th>
          <th></th>
          </tr>
          </thead>
          <tbody>
              {this.renderRealEstates()}
          </tbody>
          </Table>
          </Col></Row>
          <Row>
              <Col>
              <Button disabled={this.state.pageNo===0} 
                onClick={()=>this.getRealEstates(this.state.pageNo-1)}
                className="mr-3">Prethodna</Button>
              <Button disabled={this.state.pageNo==this.state.totalPages-1} 
              onClick={()=>this.getRealEstates(this.state.pageNo+1)}>Sledeca</Button>
              </Col>
          </Row>
      </Col>
  );
}
}


    export default withNavigation(withParams(RealEstates));