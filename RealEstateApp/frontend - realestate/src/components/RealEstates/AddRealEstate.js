import React from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import { withNavigation, withParams } from "../../routeconf";
import AppAxios from "../../apis/AppAxios";

class DodajVino extends React.Component {
  constructor(props) {
    super(props);

    let vino = {
      naziv: "",
      opis: "",
      godinaProizvodnje: "",
      cena:"",
      vinarijaId: "",
      tipVinaId: "",

    };

    
    this.state = {
      vino: vino,
      tipovi: [],
      vinarije: [],
    };
    this.create = this.create.bind(this);
  }

  componentDidMount() {
    this.getTipoviVina();
    this.getVinarije();
    }

    async getTipoviVina(){
        AppAxios.get("/tipovi")
        .then((response)=>{
            this.setState({tipovi:response.data});
        })
        .catch((err=>{console.log(err)}));
    }

    async getVinarije(){
      AppAxios.get("/vinarije")
      .then((response)=>{
          this.setState({vinarije:response.data});
      })
      .catch((err=>{console.log(err)}));
  }

async create() {
  let params = {
    "naziv" : this.state.vino.naziv,
    "opis": this.state.vino.opis,
    "godinaProizvodnje": this.state.vino.godinaProizvodnje,
    "cena" : this.state.vino.cena,
    "vinarijaId" : this.state.vino.vinarijaId,
    "tipVinaId" : this.state.vino.tipVinaId,


  };
  await AppAxios.post("/vina", params)
  .then((res) => {
    // handle success
    console.log(res);

    alert("Product was added successfully!");
    this.props.navigate("/vina");
  })
  .catch((error) => {
    // handle error
    console.log(error);
    alert("Error occured please try again!");
  });
}


    onInputChange(event) {
      console.log(this.state.vino)
      const name = event.target.name;
      const value = event.target.value;
  
      console.log(value)
  
      let vino = this.state.vino;
      vino[name] = value;
  
      this.setState({ vino })
  }


    render() {
      return (
        <>
          <Row>
            <Col></Col>
            <Col xs="12" sm="10" md="8">
              <h1>Dodavanje novog proizvoda</h1>
              <Form>
                <Form.Label htmlFor="naziv">Naziv vina</Form.Label>
                <Form.Control
                  placeholder="Naziv vina"
                  name="naziv"
                  type="text"
                  onChange={(e) => this.onInputChange(e)}
                />
                <Form.Label htmlFor="opis">Opis vina</Form.Label>
                <Form.Control
                  placeholder="Opis vina"
                  name="opis"
                  type="text"
                  onChange={(e) => this.onInputChange(e)}
                />
                <Form.Label htmlFor="godinaProizvodnje">Godina proizvodnje</Form.Label>
                <Form.Control
                  placeholder="Godina proizvodnje"
                  name="godinaProizvodnje"
                  type="number"
                  onChange={(e) => this.onInputChange(e)}
                />
                <Form.Label htmlFor="cena">Cena po flasi</Form.Label>
                <Form.Control
                  placeholder="Cena po flasi"
                  name="cena"
                  type="number"
                  onChange={(e) => this.onInputChange(e)}
                />
                <Form.Group>
                      <Form.Label>Tip vina</Form.Label>
                      <Form.Control name="tipVinaId" 
                      as="select"
                      type="number"
                      onChange={(e)=>this.onInputChange(e)}>
                          <option value=""></option>
                          {this.state.tipovi.map((tipovi)=>{
                              return(
                                  <option value={tipovi.id}>{tipovi.naziv}</option>
                              );
                          })}
                      </Form.Control>
                  </Form.Group>
                  <Form.Group>
                      <Form.Label>Vinarija</Form.Label>
                      <Form.Control name="vinarijaId" 
                      as="select"
                      type="number"
                      onChange={(e)=>this.onInputChange(e)}>
                          <option value=""></option>
                          {this.state.vinarije.map((vinarije)=>{
                              return(
                                  <option value={vinarije.id}>{vinarije.naziv}</option>
                              );
                          })}
                      </Form.Control>
                  </Form.Group>
                <Button style={{ marginTop: "25px" }} onClick={this.create}>
                  Dodajte
                </Button>
              </Form>
            </Col>
            <Col></Col>
          </Row>
        </>
      );
  }
}

export default withNavigation(withParams(DodajVino));