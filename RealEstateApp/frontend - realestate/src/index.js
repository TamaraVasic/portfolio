import React from "react";
import ReactDOM from "react-dom";
import {
  Route,
  Link,
  HashRouter as Router,
  Routes,
  Navigate,
} from "react-router-dom";
import Home from "./components/Home";
import { Container, Navbar, Nav, Button } from "react-bootstrap";
import Login from "./components/Login/Login";
import { logout } from "./services/auth";
import Vina from "./components/RealEstates/RealEstates";
import DodajVino from "./components/RealEstates/AddRealEstate";
import NotFound from './components/NotFound';
import RealEstates from "./components/RealEstates/RealEstates";
import AddRealEstate from "./components/RealEstates/AddRealEstate";

class App extends React.Component {
  render() {

    const jwt = window.localStorage['jwt'];

    if(jwt){
      return (
        <div>
          <Router>
            <Navbar bg="dark" variant="dark" expand>
              <Navbar.Brand as={Link} to="/">
                JWD
              </Navbar.Brand>
              {/*className="mr-auto" podesi ovu grupu Nav Link-ova da se "rasire" sto je vise moguce,
              i zbog toga je dugme Log in/Log out skroz sa leve strane*/}
              <Nav className="mr-auto">
                <Nav.Link as={Link} to="/realestates">Nekretnine </Nav.Link>
                <Nav.Link as={Link} to="/realestates/add">Dodaj nekretnine </Nav.Link>
                <Button onClick = {()=>logout()}>Log out</Button> 
              </Nav>
              </Navbar>

            <Container style={{marginTop:25}}>
              <Routes>
                <Route path="/" element={<Home/>} />
                <Route path="/realestates" element={<RealEstates/>} />
                <Route path="/realestates/add" element={<AddRealEstate/>} />
                <Route path="/login" element={<Login/>}/>
                <Route path="*" element={<NotFound/>} />
              </Routes>
            </Container>
          </Router>
        </div>
        
      );
            }else{
              return(
              
                <div>
                <Router>
                  <Navbar bg="dark" variant="dark" expand>
                    <Navbar.Brand as={Link} to="/">
                      JWD
                    </Navbar.Brand>
                    {/*className="mr-auto" podesi ovu grupu Nav Link-ova da se "rasire" sto je vise moguce,
                    i zbog toga je dugme Log in/Log out skroz sa leve strane*/}
                    <Nav className="mr-auto">
                      <Nav.Link as={Link} to="/realestates">Nekretnine </Nav.Link>
                      <Nav.Link as={Link} to="/login"> Login</Nav.Link>
                    </Nav>
                    </Navbar>
      
                  <Container style={{marginTop:25}}>
                    <Routes>
                      <Route path="/" element={<Home/>} />
                      <Route path="/realestates" element={<RealEstates/>} />
                      <Route path="/login" element={<Login/>}/>
                      <Route path="*" element={<Navigate replace to = "/login"/>} />
                    </Routes>
                  </Container>
                </Router>
              </div>
);
                  }






  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
